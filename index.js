import nodemailer from "nodemailer"
import Mailgen from "mailgen"
import dotenv from "dotenv"

dotenv.config()

const transporter = nodemailer.createTransport({
	host: process.env.mailHost,
	port: process.env.mailPort,
	auth: {
		user: process.env.mailUser,
		pass: process.env.mailPass
	}
})

// Mailgen konfigurieren, wir nehmen das default theme
const mGen = new Mailgen({
	theme: "default",
	product: {
		name: "Nodemailer Test Mail",
		link: "https://mailgen.js",
	}
})

// Konfiguration des Inhalts
const email = {
	body: {
		name: "NODE JS Kurs",
		intro: "Wir testen Mails mit nodemailer",
		outro: "Sonst noch Fragen?"
	}
}

// den Mailbody erstellen aus dem template
const mailBody = mGen.generate(email)

async function mailMe() {
	const resp = await transporter.sendMail({
		from: `"NODEJS Kurs" ${process.env.mailFrom}`,
		to: "machernator@gmail.com",
		subject: "Wir testen Mails mit nodemailer",
		text: "Test erfolgreich!",
		html: mailBody
	})

	console.log(resp);
}

mailMe().catch(console.error)
